#include <iostream>

#include <SFML/Graphics.hpp>

#include <tmxlite/Map.hpp>

#include "animation.hpp"
#include "animated_sprite.hpp"
#include "map.hpp"
#include "player.hpp"


int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Project Codename Bash", sf::Style::Default);
    window.setVerticalSyncEnabled(true);

    Map layer_zero;
    layer_zero.load("assets/maps/beach_min.tmx", 0);

    Player player;

    player.set_sprite_sheet("assets/characters/motw.png");
    player.set_walking_animation_down(52, 72, 6, 4, 3, 1);
    player.set_walking_animation_left(52, 72, 6, 5, 3, 1);
    player.set_walking_animation_right(52, 72, 6, 6, 3, 1);
    player.set_walking_animation_up(52, 72, 6, 7, 3, 1);

    player.init_sprite(sf::Vector2f(800/2, 600/2));

    sf::Clock frame_clock;
    bool no_key_was_pressed = true;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
                window.close();
        }

        sf::Time frame_time = frame_clock.restart();

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        {
            player.set_movement(Movement::up);
            no_key_was_pressed = false;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        {
            player.set_movement(Movement::down);
            no_key_was_pressed = false;
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        {
            player.set_movement(Movement::left);
            no_key_was_pressed = false;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        {
            player.set_movement(Movement::right);
            no_key_was_pressed = false;
        }
        player.move(frame_time);

        if (no_key_was_pressed)
        {
            player.set_movement(Movement::none);
        }
        no_key_was_pressed = true;

        player.update(frame_time);

        window.clear();
        window.draw(layer_zero);
        player.draw(window);
        window.display();
    }

    return 0;
}