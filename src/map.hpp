/*********************************************************************
Based on:
**********************************************************************
Matt Marchant 2016
http://trederia.blogspot.com

tmxlite - Zlib license.
*********************************************************************/

#ifndef DEFINE_MAP
#define DEFINE_MAP

#include <memory>
#include <vector>
#include <array>
#include <map>
#include <string>
#include <limits>
#include <iostream>
#include <cmath>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Transformable.hpp>

#include <tmxlite/Map.hpp>
#include <tmxlite/TileLayer.hpp>

#include "chunk.hpp"


class Map : public sf::Drawable {
public:
    Map();

    /**
    *  @brief Load tmx map.
    *  @param filename is the path to the tiled tmx map file.
    *  @param idx is the layer id.
    */
    void load(const std::string &filename, std::size_t idx);

    /**
    *  @brief Create chunks.
    *  @param layer
    */
    void create_chunks(const tmx::TileLayer &layer);

    /**
    *  @brief Update visibility
    *  @param view
    */
    void update_visibility(const sf::View &view) const;

    /**
    *  @brief Override draw from sf::Drawable.
    *  @param rt
    *  @param states
    */
    void draw(sf::RenderTarget &rt, sf::RenderStates states) const override;

private:
    tmx::Map m_map;

    std::vector<Chunk::Ptr> m_chunks;
    sf::Vector2f m_chunk_size = sf::Vector2f(4096.f, 4096.f);
    sf::Vector2u m_chunk_count;
    mutable std::vector<const Chunk *> m_visible_chunks;

    sf::FloatRect m_global_bounds;

    TextureResource m_texture_resource;
};
#endif //DEFINE_MAP