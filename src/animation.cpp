#include "animation.hpp"


Animation::Animation() : m_texture(NULL) {

}

void Animation::add_frame(sf::IntRect rect) {
    m_frames.push_back(rect);
}

void Animation::add_frames(int width, int height, int start_x, int start_y, int count_x, int count_y) {
    for(int i = 0; i < count_x; ++i) {
        for(int j = 0; j < count_y; ++j) {
            add_frame(sf::IntRect(start_x * width + i * width, start_y * height + j * height, width, height));
            }
    }
}

void Animation::set_sprite_sheet(const sf::Texture& texture) {
    m_texture = &texture;
}

const sf::Texture* Animation::get_sprite_sheet() const {
    return m_texture;
}

std::size_t Animation::get_size() const {
    return m_frames.size();
}

const sf::IntRect& Animation::get_frame(std::size_t n) const {
    return m_frames[n];
}