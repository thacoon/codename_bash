#include "player.hpp"


Player::Player()
: m_animated_sprite(sf::seconds(0.2), true, false)
, m_speed(80.f)
, m_movement(0.f, 0.f)
{
}

void Player::set_sprite_sheet(const std::string &filename) {
    if (!m_texture.loadFromFile("assets/characters/motw.png")) {
        std::cout << "Failed to load player spritesheet!" << std::endl;
    }
}

void Player::set_walking_animation_down(int width, int height, int start_x, int start_y, int count_x, int count_y) {
    m_walking_animation_down.set_sprite_sheet(m_texture);
    m_walking_animation_down.add_frames(width, height, start_x, start_y, count_x, count_y);
}

void Player::set_walking_animation_left(int width, int height, int start_x, int start_y, int count_x, int count_y) {
    m_walking_animation_left.set_sprite_sheet(m_texture);
    m_walking_animation_left.add_frames(width, height, start_x, start_y, count_x, count_y);
}

void Player::set_walking_animation_right(int width, int height, int start_x, int start_y, int count_x, int count_y) {
    m_walking_animation_right.set_sprite_sheet(m_texture);
    m_walking_animation_right.add_frames(width, height, start_x, start_y, count_x, count_y);
}

void Player::set_walking_animation_up(int width, int height, int start_x, int start_y, int count_x, int count_y) {
    m_walking_animation_up.set_sprite_sheet(m_texture);
    m_walking_animation_up.add_frames(width, height, start_x, start_y, count_x, count_y);
}

void Player::init_sprite(sf::Vector2f pos) {
    m_current_animation = &m_walking_animation_down;
    m_animated_sprite.setPosition(pos);
}

void Player::set_movement(Movement direction) {
    if(direction == Movement::up) {
        m_current_animation = &m_walking_animation_up;
        m_movement.y -= m_speed;
    }
    else if(direction == Movement::down) {
        m_current_animation = &m_walking_animation_down;
        m_movement.y += m_speed;
    }
    else if(direction == Movement::left) {
        m_current_animation = &m_walking_animation_left;
        m_movement.x -= m_speed;
    }
    else if(direction == Movement::right) {
        m_current_animation = &m_walking_animation_right;
        m_movement.x += m_speed;
    }
    else if(direction == Movement::none){
        m_animated_sprite.stop();
    }
}

void Player::move(sf::Time delta_time) {
    m_animated_sprite.play(*m_current_animation);
    m_animated_sprite.move(m_movement * delta_time.asSeconds());

    m_movement = sf::Vector2f(0.f, 0.f);
}

void Player::update(sf::Time delta_time) {
    m_animated_sprite.update(delta_time);
}

void Player::draw(sf::RenderWindow &window) {
    window.draw(m_animated_sprite);
}