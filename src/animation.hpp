#ifndef DEFINE_ANIMATION
#define DEFINE_ANIMATION

#include <vector>

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Texture.hpp>


/**
* Based on 2014 Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de)
* https://github.com/SFML/SFML/wiki/Source:-AnimatedSprite
*/
class Animation {
public:
    Animation();

    /**
    *  @brief Add a frame to the animation.
    *  @param rect is the rectangular position in the sprite.
    */
    void add_frame(sf::IntRect rect);

    /**
    *  @brief Add multiple frames.
    *  @param width is the width of a single frame.
    *  @param height is the height of a single frame.
    *  @param start_x is the position in x-axis of the frame in the sprite sheet mminus 1.
    *  @param start_y is the position in y-axis of the frame in the sprite sheet mminus 1.
    *  @param count_x is the number of frames in x direction for the defined animation.
    *  @param count_y is the number of frames in y direction for the defined animation.
    */
    void add_frames(int width, int height, int start_x, int start_y, int count_x, int count_y);

    /**
    *  @brief Set the sprite of the animation
    *  @param texture is a reference to the sf::Texture.
    */
    void set_sprite_sheet(const sf::Texture& texture);

    /**
    *  @return sf::Texture pointer to the set sprite texture.
    */
    const sf::Texture* get_sprite_sheet() const;

    /**
    *  @return the count of frames.
    */
    std::size_t get_size() const;

    /**
    *  @brief Get the count of frames.
    *  @param n is the current frame to get.
    *  @return the frame at position n.
    */
    const sf::IntRect& get_frame(std::size_t n) const;

private:
    std::vector<sf::IntRect> m_frames;
    const sf::Texture* m_texture;
};

#endif // DEFINE_ANIMATION