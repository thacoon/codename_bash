#ifndef DEFINE_PLAYER
#define DEFINE_PLAYER

#include <iostream>

#include <SFML/Graphics.hpp>

#include "animation.hpp"
#include "animated_sprite.hpp"


enum class Movement {up, down, left, right, none};


class Player {
public:
    Player();

    /**
    *  @brief Set a sprite sheet for the character.
    *  @param filename is the path to the sprite sheet file.
    */
    void set_sprite_sheet(const std::string &filename);

   /**
    *  @brief Add the animation for walking down.
    *  @param width is the width of a single frame.
    *  @param height is the height of a single frame.
    *  @param start_x is the position in x-axis of the frame in the sprite sheet mminus 1.
    *  @param start_y is the position in y-axis of the frame in the sprite sheet mminus 1.
    *  @param count_x is the number of frames in x direction for the defined animation.
    *  @param count_y is the number of frames in y direction for the defined animation.
    */
    void set_walking_animation_down(int width, int height, int start_x, int start_y, int count_x, int count_y);

   /**
    *  @brief Add the animation for walking down.
    *  @param see set_walking_animation_down
    */
    void set_walking_animation_left(int width, int height, int start_x, int start_y, int count_x, int count_y);

   /**
    *  @brief Add the animation for walking down.
    *  @param see set_walking_animation_down
    */
    void set_walking_animation_right(int width, int height, int start_x, int start_y, int count_x, int count_y);

   /**
    *  @brief Add the animation for walking down.
    *  @param see set_walking_animation_down
    */
    void set_walking_animation_up(int width, int height, int start_x, int start_y, int count_x, int count_y);

    /**
    *  @brief Init the sprite. This should be done after set all animations.
    *  @param pos is the initial position to draw the sprite.
    */
    void init_sprite(sf::Vector2f pos);

    /**
    *  @brief Set the direction of the movement.
    *  @param direction
    */
    void set_movement(Movement direction);

    /**
    *  @brief Move the sprite and reset movement vector.
    *  @param delta_time is the time since last call
    */
    void move(sf::Time delta_time);

    /**
    *  @brief Update the sprite
    *  @param delta_time is the time since last call
    */
    void update(sf::Time delta_time);

    /**
    *  @brief Draw the player sprite to the screen.
    *  @param window is a reference to the render target.
    */
    void draw(sf::RenderWindow &window);

private:
    sf::Texture m_texture;

    Animation m_walking_animation_down;
    Animation m_walking_animation_left;
    Animation m_walking_animation_right;
    Animation m_walking_animation_up;

    Animation *m_current_animation;

    AnimatedSprite m_animated_sprite;

    float m_speed;
    sf::Vector2f m_movement;
};

#endif //DEFINE_PLAYER