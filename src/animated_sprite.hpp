#ifndef DEFINE_ANIMATEDSPRITE
#define DEFINE_ANIMATEDSPRITE

#include <iostream>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/Vector2.hpp>

#include "animation.hpp"


/**
* Based on 2014 Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de)
* https://github.com/SFML/SFML/wiki/Source:-AnimatedSprite
*/
class AnimatedSprite : public sf::Drawable, public sf::Transformable {
public:
    explicit AnimatedSprite(sf::Time frameTime = sf::seconds(0.2f), bool paused = false, bool looped = true);

    /**
    *  @brief Set animation to frame 0.
    *  @param animation is a reference to a Animation object.
    */
    void set_animation(const Animation& animation);

    /**
    *  @brief Set time.
    *  @param time is the time to set to.
    */
    void set_frame_time(sf::Time time);

    /**
    *  @brief Start playing the animation.
    */
    void play();

    /**
    *  @brief Start playing a specific animation.
    *  @param animation is a new Animation to play.
    */
    void play(const Animation& animation);

    /**
    *  @brief Pause the animation.
    */
    void pause();

    /**
    *  @brief Pause the animation and set reset the current frame.
    */
    void stop();

    /**
    *  @brief Set the animation to be looped.
    *  @param looped loop the animation on true otherwise use false.
    */
    void set_looped(bool looped);

    /**
    *  @brief Set color of vertices.
    *  @param color is the new color for the vertices.
    */
    void set_color(const sf::Color& color);

    /**
    * @return the current animation.
    */
    const Animation* get_animation() const;

    /**
    *  @return a FloatRect of the local bounds of the current frame.
    */
    sf::FloatRect get_local_bounds() const;

    /**
    *  @return a FloatRect of the global bounds of the current frame.
    */
    sf::FloatRect get_global_bounds() const;

    /**
    *  @return true if animation is looped otherwise false.
    */
    bool is_looped() const;

    /**
    *  @return true if animation is played otherwise false..
    */
    bool is_playing() const;

    /**
    *  @return the current frame time.
    */
    sf::Time get_frame_time() const;

    /**
    *  @brief Calculate the new vertex positions and texture coordinates.
    *  @param new_frame is the frame to set.
    *  @param reset_time do not reset the time if false, default is true.
    */
    void set_frame(std::size_t new_frame, bool reset_time = true);

    /**
    *  @brief If animation is not paused update the frames according to the passed time.
    *  @details Resets the current time but keeps the remainder of current time and passed time as the new current time.
    *  @param delta_time the time passed since last call.
    */
    void update(sf::Time deltaTime);

private:
    const Animation* m_animation;
    sf::Time m_frame_time;
    sf::Time m_current_time;
    std::size_t m_current_frame;
    bool m_is_paused;
    bool m_is_looped;
    const sf::Texture* m_texture;
    sf::Vertex m_vertices[4];

    /**
    *  @brief Draw the texture to a defined target
    *  @param target is the render target, e.g. the window.
    *  @param states are the render states.
    */
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

};

#endif // DEFINE_ANIMATEDSPRITE