#include "animated_sprite.hpp"


AnimatedSprite::AnimatedSprite(sf::Time frame_time, bool paused, bool looped) :
    m_animation(NULL), m_frame_time(frame_time), m_current_frame(0), m_is_paused(paused), m_is_looped(looped), m_texture(NULL)
{

}

void AnimatedSprite::set_animation(const Animation& animation)
{
    m_animation = &animation;
    m_texture = m_animation->get_sprite_sheet();
    m_current_frame = 0;
    set_frame(m_current_frame);
}

void AnimatedSprite::set_frame_time(sf::Time time)
{
    m_frame_time = time;
}

void AnimatedSprite::play()
{
    m_is_paused = false;
}

void AnimatedSprite::play(const Animation& animation)
{
    if (get_animation() != &animation)
        set_animation(animation);
    play();
}

void AnimatedSprite::pause()
{
    m_is_paused = true;
}

void AnimatedSprite::stop()
{
    m_is_paused = true;
    m_current_frame = 0;
    set_frame(m_current_frame);
}

void AnimatedSprite::set_looped(bool looped)
{
    m_is_looped = looped;
}

void AnimatedSprite::set_color(const sf::Color& color)
{
    m_vertices[0].color = color;
    m_vertices[1].color = color;
    m_vertices[2].color = color;
    m_vertices[3].color = color;
}

const Animation* AnimatedSprite::get_animation() const
{
    return m_animation;
}

sf::FloatRect AnimatedSprite::get_local_bounds() const
{
    sf::IntRect rect = m_animation->get_frame(m_current_frame);

    float width = static_cast<float>(std::abs(rect.width));
    float height = static_cast<float>(std::abs(rect.height));

    return sf::FloatRect(0.f, 0.f, width, height);
}

sf::FloatRect AnimatedSprite::get_global_bounds() const
{
    return getTransform().transformRect(get_local_bounds());
}

bool AnimatedSprite::is_looped() const
{
    return m_is_looped;
}

bool AnimatedSprite::is_playing() const
{
    return !m_is_paused;
}

sf::Time AnimatedSprite::get_frame_time() const
{
    return m_frame_time;
}

void AnimatedSprite::set_frame(std::size_t new_frame, bool reset_time)
{
    if (m_animation)
    {
        sf::IntRect rect = m_animation->get_frame(new_frame);

        m_vertices[0].position = sf::Vector2f(0.f, 0.f);
        m_vertices[1].position = sf::Vector2f(0.f, static_cast<float>(rect.height));
        m_vertices[2].position = sf::Vector2f(static_cast<float>(rect.width), static_cast<float>(rect.height));
        m_vertices[3].position = sf::Vector2f(static_cast<float>(rect.width), 0.f);

        float left = static_cast<float>(rect.left) + 0.0001f;
        float right = left + static_cast<float>(rect.width);
        float top = static_cast<float>(rect.top);
        float bottom = top + static_cast<float>(rect.height);

        m_vertices[0].texCoords = sf::Vector2f(left, top);
        m_vertices[1].texCoords = sf::Vector2f(left, bottom);
        m_vertices[2].texCoords = sf::Vector2f(right, bottom);
        m_vertices[3].texCoords = sf::Vector2f(right, top);
    }

    if (reset_time)
        m_current_time = sf::Time::Zero;
}

void AnimatedSprite::update(sf::Time delta_time)
{
    if (!m_is_paused && m_animation)
    {
        m_current_time += delta_time;

        if (m_current_time >= m_frame_time)
        {
            m_current_time = sf::microseconds(m_current_time.asMicroseconds() % m_frame_time.asMicroseconds());

            if (m_current_frame + 1 < m_animation->get_size())
                m_current_frame++;
            else
            {
                m_current_frame = 0;

                if (!m_is_looped)
                {
                    m_is_paused = true;
                }

            }

            set_frame(m_current_frame, false);
        }
    }
}

void AnimatedSprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if (m_animation && m_texture)
    {
        states.transform *= getTransform();
        states.texture = m_texture;
        target.draw(m_vertices, 4, sf::Quads, states);
    }
}