#include "map.hpp"

Map::Map() {
}

void Map::load(const std::string &filename, std::size_t idx) {
    if(m_map.load(filename)) {
        std::cout << "Loaded Map version: " << m_map.getVersion().upper << ", " << m_map.getVersion().lower << std::endl;
    }
    else {
        std::cout << "Failed loading map" << filename << std::endl;
    }

    const std::vector<tmx::Layer::Ptr>& layers = m_map.getLayers();

    if(m_map.getOrientation() == tmx::Orientation::Orthogonal &&
       idx < layers.size() && layers[idx]->getType() == tmx::Layer::Type::Tile) {
        const tmx::Vector2u& tile_size = m_map.getTileSize();

        m_chunk_size.x = std::floor(m_chunk_size.x / tile_size.x) * tile_size.x;
        m_chunk_size.y = std::floor(m_chunk_size.y / tile_size.y) * tile_size.y;

        const tmx::TileLayer layer = *dynamic_cast<const tmx::TileLayer *>(layers[idx].get());
        create_chunks(layer);

        tmx::FloatRect map_size = m_map.getBounds();
        m_global_bounds.width = map_size.width;
        m_global_bounds.height = map_size.height;
    }
    else {
        std::cout << "Not a valid orthogonal layer" << std::endl;
    }
}

void Map::create_chunks(const tmx::TileLayer &layer) {
    const std::vector<tmx::Tileset>& tile_sets = m_map.getTilesets();
    const std::vector<tmx::TileLayer::Tile> layer_ids = layer.getTiles();

    std::uint32_t max_id = std::numeric_limits<std::uint32_t>::max();
    std::vector<const tmx::Tileset*> used_tile_sets;

    for(auto i = tile_sets.rbegin(); i != tile_sets.rend(); ++i) {
        for(tmx::TileLayer::Tile tile : layer_ids) {
            if(tile.ID >= i->getFirstGID() && tile.ID < max_id) {
                used_tile_sets.push_back(&(*i));
                break;
            }
        }
        max_id = i->getFirstGID();
    }

    sf::Image fallback;
    fallback.create(2, 2, sf::Color::Magenta);
    for(const tmx::Tileset *ts : used_tile_sets) {
        const std::string& path = ts->getImagePath();

        std::unique_ptr<sf::Texture> new_texture = std::unique_ptr<sf::Texture>(new sf::Texture());

        sf::Image img;
        if(!img.loadFromFile(path)) {
            new_texture->loadFromImage(fallback);
        }
        else {
            if(ts->hasTransparency()) {
                const tmx::Colour&  transperency = ts->getTransparencyColour();
                img.createMaskFromColor({transperency.r, transperency.g, transperency.b, transperency.a});
            }
            new_texture->loadFromImage(img);
        }
        m_texture_resource.insert(std::make_pair(path, std::move(new_texture)));
    }

    const tmx::FloatRect bounds = m_map.getBounds();
    m_chunk_count.x = static_cast<sf::Uint32>(std::ceil(bounds.width / m_chunk_size.x));
    m_chunk_count.y = static_cast<sf::Uint32>(std::ceil(bounds.height / m_chunk_size.y));

    sf::Vector2f tile_count(m_chunk_size.x / m_map.getTileSize().x, m_chunk_size.y / m_map.getTileSize().y);

    for(sf::Uint32 y = 0u; y < m_chunk_count.y; ++y) {
        for(sf::Uint32 x = 0u; x < m_chunk_count.x; ++x) {
            m_chunks.emplace_back(std::unique_ptr<Chunk>(new Chunk(layer, used_tile_sets,
            sf::Vector2f(x * m_chunk_size.x, y * m_chunk_size.y), tile_count, m_map.getTileCount().x, m_texture_resource)));
        }
    }
}

void Map::update_visibility(const sf::View &view) const{
    sf::Vector2f view_corner = view.getCenter();
    view_corner -= view.getSize() / 2.f;

    int pos_x = static_cast<int>(std::floor(view_corner.x / m_chunk_size.x));
    int pos_y = static_cast<int>(std::floor(view_corner.y / m_chunk_size.y));

    std::vector<const Chunk*> visible;
    for(int y = pos_y; y < pos_y + 2; ++y) {
        for(int x = pos_x; x < pos_x + 2; ++x) {
            int idx = y * int(m_chunk_count.x) + x;
            if(idx >= 0 && idx < m_chunks.size() && !m_chunks[idx]->empty()) {
                visible.push_back(m_chunks[idx].get());
            }
        }
    }

    std::swap(m_visible_chunks, visible);
}

void Map::draw(sf::RenderTarget &rt, sf::RenderStates states) const {
    update_visibility(rt.getView());
    for(const Chunk * c : m_visible_chunks) {
        rt.draw(*c, states);
    }
}