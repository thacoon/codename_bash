Codename Bash
=============


## Requirements
```
# tmxlite (https://github.com/fallahn/tmxlite)
# Download and unzip tmxlite to tmxlite
$ cd tmxlite/tmxlite/
$ mkdir /home/username/opt
$ cmake -D CMAKE_INSTALL_PREFIX:PATH=/home/$(USER)/opt/ .
$ make install
$ export LD_LIBRARY_PATH=/home/username/opt/lib:$LD_LIBRARY_PATH
```