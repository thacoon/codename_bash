CC = g++
RM = rm

CFlags = -std=c++14 -Wall -g -c -I/home/${USERNAME}/opt/include
LDFlags = -lsfml-graphics -lsfml-window -lsfml-system -L/home/${USERNAME}/opt/lib -ltmxlite
LDTFlags = $(LDFlags) -lgtest -lgmock -lpthread -lgtest_main
ObjectDir = obj/
SourceDir = src/
TestDir = tests/
BinDir = bin/

Sources =  animation.cpp animated_sprite.cpp main.cpp map.cpp player.cpp
SourcesTest =

Executable = project_bash
ExecutableTest = test_project_bash

Objects = $(Sources:.cpp=.o)
ObjectsTest = $(SourcesTest:.cpp=.o)

CSources = $(addprefix $(SourceDir),$(Sources))
CSourcesTest = $(addprefix $(TestDir),$(SourcesTest))

CObjects = $(addprefix $(ObjectDir),$(Objects))
CObjectsToTest = $(filter-out obj/main.o, $(CObjects))
CObjectsTest = $(addprefix $(ObjectDir),$(ObjectsTest))

CExecutable = $(addprefix $(BinDir),$(Executable))
CExecutableTest = $(addprefix $(BinDir),$(ExecutableTest))

all: $(CSources) $(CExecutable)

test: $(CSourcesTest) $(CExecutableTest)

$(CExecutable): $(CObjects)
	$(CC) $(LDFlags) $(CObjects) -o $@

$(CExecutableTest): $(CObjectsToTest) $(CObjectsTest)
	$(CC) $(LDTFlags) $(CObjectsToTest) $(CObjectsTest) -o $@

$(ObjectDir)%.o: $(SourceDir)%.cpp
	$(CC) $(CFlags) $< -o $@

$(ObjectDir)%.o: $(TestDir)%.cpp
	$(CC) $(CFlags) $< -o $@

clean:
	$(RM) -f $(CObjects) $(CObjectsTest)
